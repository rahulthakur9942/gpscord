﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GPSCord
{
    class Program
    {
		public static string LatitudeText = "";
		public static string LongitudeText = "";
		public static string Radius = "";
		static void Main(string[] args)
		{
	
		
	
			ReadData();

			SerialPort mySerialPort = new SerialPort("COM7");
			mySerialPort.BaudRate = 4800;
			mySerialPort.Parity = Parity.None;
			mySerialPort.StopBits = StopBits.One;
			mySerialPort.DataBits = 8;
			mySerialPort.Handshake = Handshake.None;
			mySerialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

			mySerialPort.Open();
			Console.WriteLine("Checking GPS...");
			Console.WriteLine();
			Console.ReadKey();
			mySerialPort.Close();
		}

		public static void ReadData()
		{
	

	
			try
			{

			
			
			string textFile = @"C:\Temp\com.txt";
			if (File.Exists(textFile))
			{
				// Read a text file line by line.
				string[] lines = File.ReadAllLines(textFile);
				string[] SplitLineVal = lines[0].Split(';');
				LatitudeText = SplitLineVal[0];
				LongitudeText = SplitLineVal[1];
				Radius = SplitLineVal[2];
			}
			}
			catch 
			{

	
			}
		}
			static	int Count = 0;
		private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {


                string Latitude = "";
                string Longitude = "";
                SerialPort sp = (SerialPort)sender;
                string indata = sp.ReadLine();
                // Debug.Print("Data Received:");
                if (indata.Contains("$GPGGA"))
                {

                    string[] ArrSplit = indata.Split(',');
                    Latitude = ArrSplit[2];
                    Longitude = ArrSplit[4];
                    string LatContainer = (Latitude.ToCharArray())[0] + (Latitude.ToCharArray())[1].ToString();
                    string LongContainer = (Longitude.ToCharArray())[1] + (Longitude.ToCharArray())[2].ToString();

                    string LeftLat = Latitude;
                    StringBuilder sblat = new StringBuilder(LeftLat);
                    sblat.Remove(0, 2);
                    LeftLat = sblat.ToString();
                    decimal CalLeftlat = Convert.ToDecimal(LeftLat) / 60;

                    string LeftLong = Longitude;
                    StringBuilder sblong = new StringBuilder(LeftLong);
                    sblong.Remove(0, 3);
                    LeftLong = sblong.ToString();
                    decimal CalLeftlong = Convert.ToDecimal(LeftLong) / 60;

                    decimal ConcatLat = Convert.ToDecimal(LatContainer) + CalLeftlat;
                    decimal ConcatLong = Convert.ToDecimal(LongContainer) + CalLeftlong;

					//Console.WriteLine(ConcatLat);
					//Console.WriteLine(ConcatLong);
					string path = "C:\\temp\\latlong.txt";
					if (!File.Exists(path))
					{
						//File.Create(path);
						var myFile = File.Create(path);
						myFile.Close();
					}
				
					using (StreamWriter newTask = new StreamWriter(path, false))
					{
						newTask.WriteLine(ConcatLat.ToString().Substring(0, 7) + "," + ConcatLong.ToString().Substring(0, 7)); //write data
						newTask.Close();

					}
		
					//int flag =0;

					//double Distance = GetDistance(Double.Parse(ConcatLat.ToString()), Double.Parse(ConcatLong.ToString()), Double.Parse(LatitudeText), Double.Parse(LongitudeText));
					//flag = Convert.ToInt32(Distance < Convert.ToDouble(Radius) ? 1 : 0);
					////if (ChkRadius==1)
					////{
					////	flag = ConcatLat.ToString().Substring(0, 6) == LatitudeText.Substring(0, 6) && ConcatLong.ToString().Substring(0, 6) == LongitudeText.Substring(0, 6) ? 1 : 0;
					////}

					//string fileName = @"C:\Temp\IsComTrue.txt";
					//if (!File.Exists(fileName))
					//{
					//	//File.Create(path);
					//	var myFile = File.Create(fileName);
					//	myFile.Close();
					//}
					//using (StreamWriter newTask = new StreamWriter(fileName, false))
					//{

					//	newTask.WriteLine(flag);
					//	newTask.Dispose();
					//	newTask.Close();
					//}

					if (Count > 5)
					{
						Thread.Sleep(2000);
						Environment.Exit(0);
					}
					Count++;
			
				}
		

			}
            catch
            {


            }
        }

		public static double GetDistance(double lat1, double lon1, double lat2, double lon2)
		{

			try
			{

				var R = 6371; // Radius of the earth in km
				var dLat = ToRadians(lat2 - lat1);  // deg2rad below
				var dLon = ToRadians(lon2 - lon1);
				var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
					Math.Cos(ToRadians(lat1)) * Math.Cos(ToRadians(lat2)) *
					Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

				var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
				var d = R * c; // Distance in km
				return d;
			}
			catch (Exception)
			{
				//Environment.Exit(0);
				throw;
			}
		}
		public static double ToRadians(double deg)
		{
			try
			{
				return deg * (Math.PI / 180);
			}
			catch (Exception)
			{

				throw;
			}
		}
	}
}
